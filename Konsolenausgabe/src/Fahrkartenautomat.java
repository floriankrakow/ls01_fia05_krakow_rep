import java.util.Scanner;

class Fahrkartenautomat
{
	static Scanner tastatur = new Scanner(System.in);
    public static void main(String[] args)
    {
       double zuZahlenderBetrag;
       double r�ckgabebetrag;
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen();   
       r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben();
       rueckgeldAusgeben(r�ckgabebetrag);
    	   
    }
    public static double fahrkartenbestellungErfassen() {
    	double zuZahlenderBetrag;
        double AnzahlderTickets; 
        System.out.print("Anzahl der Tickets (Tickets): ");
        AnzahlderTickets = tastatur.nextDouble();
        
        System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble() * AnzahlderTickets;
        
        
        return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneMuenze;
    	double r�ckgabeBetrag;
    	
    	System.out.printf("Noch zu zahlen: %.2f� \n", (zuZahlenderBetrag));
        eingezahlterGesamtbetrag=0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
        	System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
        	eingeworfeneMuenze = tastatur.nextDouble();
        	eingezahlterGesamtbetrag += eingeworfeneMuenze; 
        } 
        return r�ckgabeBetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    			
    
    
    public static void fahrkartenAusgeben() {
    	   System.out.println("\nFahrschein wird ausgegeben");
           for (int i = 0; i < 8; i++)
           {
              System.out.print("=");
              try {
    			Thread.sleep(250);
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
           }
           System.out.println("\n\n");

    }
    
    public static void rueckgeldAusgeben(double r�ckgabebetrag) {
        
     	   System.out.printf("Der R�ckgabebetrag in H�he von: %.2f� ", (r�ckgabebetrag));
     	   System.out.println("wird in folgenden M�nzen ausgezahlt: ");

            while(r�ckgabebetrag >= 2.00) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
 	          r�ckgabebetrag -= 2.00;
            }
            while(r�ckgabebetrag >= 1.00) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
 	          r�ckgabebetrag -= 1.00;
            }
            while(r�ckgabebetrag >= 0.50) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
 	          r�ckgabebetrag -= 0.50;
            }
            while(r�ckgabebetrag >= 0.20) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
  	          r�ckgabebetrag -= 0.20;
            }
            while(r�ckgabebetrag >= 0.10) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
 	          r�ckgabebetrag -= 0.10;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
  	          r�ckgabebetrag -= 0.05;
            }
        

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.");
    	
    }
}